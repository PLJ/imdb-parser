﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using parser.Repositories;
using System.Xml.Serialization;

namespace parser
{
    public class Task
    {
        // Define posible types of Task
        public enum TaskType
        {
            PersonExtraction,
            MovieLoad,
            PersonMovieBinding,
            PersonPersonBinding,
            MovieGenresBinding,
            MovieRatingBinding,
            RatingExtraction,
            PersonGenresBinding,
            PersonRatingBinding
        };

        // Map properties to xml atribute
        [XmlAttribute(AttributeName = "Id")]
        public int Id { get; set; }
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public TaskType Type { get; set; }
        [XmlAttribute(AttributeName = "Source")]
        public string Source { get; set; }
        [XmlAttribute(AttributeName = "Result")]
        public string Result { get; set; }
        [XmlAttribute(AttributeName = "BindingID1")]
        public int BindingID1 { get; set; }
        [XmlAttribute(AttributeName = "BindingID2")]
        public int BindingID2 { get; set; }
    }
}
