﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parser.Model
{
    public class Person
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<int> Movies { get; set; }

        public Person()
        {

        }
        public Person(int id, string name)
        {
            this.name = name;
            this.id = id;
            Movies = new List<int>();
        }
        // Print object to console
        void Print()
        {
            Console.WriteLine("ID:" + id + "; Name:" + name);
        }

        // Return List of 20 movies ordered by rating
        public List<int> GetListTopMovies( int top, MoviesRepository MovRep)
        {
            Dictionary<int, double> temp = new Dictionary<int, double>();
            List<int> result = new List<int>();

            foreach (int movie in this.Movies)
            {
                Movie mov = MovRep.GetByID(movie);
                if (mov.votes > 2000 && mov.rating > 0)
                {
                    temp.Add(movie, mov.GetWeightedRating());
                }
            }

            var items = from pair in temp
                        orderby pair.Value descending
                        select pair;

            foreach (KeyValuePair<int, double> pair in items)
            {

                result.Add(pair.Key);
                top--;
                if(top == 0)
                {
                    break;
                }
            }

            return result;
        }

        // Calc Average Weighted Ranking for person
        // This method can be used to oder persons by ranking
        public double GetAverageWeightRanking( MoviesRepository MovRep)
        {
            double WeightRatingSum = 0;
            double i = 0;

            foreach (int movie in this.Movies)
            {
                Movie mov = MovRep.GetByID(movie);
                if (mov.votes > 2000 && mov.rating > 0)
                {
                    WeightRatingSum += mov.GetWeightedRating();
                    i++;
                }
            }
            return WeightRatingSum/i;
        }
    }
}
