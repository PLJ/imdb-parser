﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parser
{
    public class Movie
    {
        public int id { get; set; }
        public string name { get; set; }
        public string year { get; set; }
        public List<int> genres { get; set; }
        public double rating { get; set; }
        public int votes { get; set; }

        public Movie(int id, string name, string year)
        {
            this.name = name;
            this.id = id;
            this.year = year;
            this.genres = new List<int>();
        }

        // Print object to console
        void Print()
        {
            Console.WriteLine("ID:" + id + "; Name:" + name + "; Year:" + year);
        }
        public Movie()
        {

        }

        // Default method for extraction from old project
        [System.Obsolete("Use extractor")]
        public static Movie TryExtract(string textLine)
        {
            if (string.IsNullOrEmpty(textLine))
                return null;

            Movie movie = new Movie();

            int p1 = textLine.IndexOf('\t', 0); // Nalezeni prvniho tabulatoru

            if (p1 != -1)
            {
                if ((movie.name = textLine.Substring(p1 )) == null) // Extrakce jmena
                    return null;

                while (++p1 < textLine.Length && textLine[p1] == '\t') // Nalezeni tabulatoru pred rokem
                    continue;

                if (p1 < textLine.Length) // Jestli nejsu na konci rezetce
                {
                    int p2 = textLine.IndexOf('\t', p1 + 1);

                    movie.year = ((p2 == -1) ? textLine.Substring(p1, textLine.Length - p1) : textLine.Substring(p1, p2 - p1));
                }
            }
            else
            {
                if ((movie.name = textLine.Substring(0, textLine.Length)) == null) // Extrakce jmena
                    return null;
            }

            //-Set-year-----

            return movie;
        }

        // Calculate WeightedRatind
        // Formula from IMDB
        public double GetWeightedRating()
        {
            double v = this.votes;
            double k = 25000;
            double X = this.rating;
            double C = 6.9;

            return (v / (v + k)) * X + (k / (v + k)) * C;
        }
    }
}
