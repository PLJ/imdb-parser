﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace parser
{
    public abstract class Extraction
    {
        // This class contains default props and methods for extraction work
        // Is designed to extend and oweride
        protected ExtractionConfig ExtConfiguration;
        protected StreamReader[] readers = null;
        protected StreamWriter writer = null;
        protected string beginString = "";
        protected string endString = "";

        // Try to open file from location for read and write
        protected bool OpenStreams(ExtractionConfig conf)
        {
            try
            {
                readers = new StreamReader[conf.Sources.Length];
                for (int i = 0; i < readers.Length; i++)
                    readers[i] = new StreamReader(new FileStream(conf.Sources[i], FileMode.Open), Encoding.GetEncoding("Windows-1250"));
            }
            catch { return false; }

            try { writer = new StreamWriter(new FileStream(conf.Destination, FileMode.Create), Encoding.GetEncoding("Windows-1250")); }
            catch { return false; }

            return true;
        }

        // Close FileStream
        protected void CloseStreams()
        {
            if (readers != null)
                for (int i = 0; i < readers.Length; i++)
                    readers[i].Close();

            if (writer != null)
                writer.Close();
        }

        // Init position in document based on beginString
        protected void InitializeStreamPositions()
        {
            if (readers == null)
                return;

            string textLine = null;
            for (int i = 0; i < readers.Length; i++)
                while ((textLine = readers[i].ReadLine()) != null && textLine != beginString)
                    continue;
        }

        // determine if is end of data section of file
        protected abstract bool EndOfExtraction(string textLine);

        // read next record
        protected abstract string GetNextRecord(StreamReader reader);
    }
}
