﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using parser.Model;
using parser.Repositories;

namespace parser
{
    public class GenresExtraction : Extraction
    {
        public string SourceFilePath;

        public GenresExtraction(ExtractionConfig conf)
        {
            // Hardcode string for genres
            beginString = "8: THE GENRES LIST";
            endString = "--";
            this.ExtConfiguration = conf;
        }


        public Dictionary<string, int> ExtractToDictionary()
        {
            Dictionary<string, int> dic = new Dictionary<string, int>();

            int id = 1;
            string textLine = null;
            string genre_name = null;
            string movie_name = null;
            HashSet<string> hashSet = new HashSet<string>();

            PersonRepository Rep = new PersonRepository();
            //This add Reader and Writer to use         
            if (!OpenStreams(ExtConfiguration))
                throw new Exception();

            // Scroll reader to start position 
            InitializeStreamPositions();
            //--------------------------------------------------

            foreach (StreamReader currentReader in readers)
            {
                textLine = currentReader.ReadLine();
                textLine = currentReader.ReadLine();

                while (((currentReader.ReadLine()) != null)  && (!EndOfExtraction(textLine = GetNextRecord(currentReader))))
                {
                  
                    if ((genre_name = ParseGenre(textLine)) == null)
                        throw new Exception("Failed to parse genre.");

                    if ((movie_name = ParseMovie(textLine)) == null)
                        throw new Exception("Failed to parse movie.");

                    if (!dic.ContainsKey(genre_name))
                    {
                        dic.Add(genre_name, id);
                        id++;
                    }
                }
            }

            CloseStreams();

            return dic;
        }

        // Try to extract genre from textline
        public string ParseGenre(string textLine)
        {

            if (string.IsNullOrEmpty(textLine) || (textLine.Length > 0 && textLine[0] == '\t'))
                return null;

            int p1 = textLine.LastIndexOf('\t');
            if (p1 == -1)
                return null;

            // return substring from last TAB to end of line
            return textLine.Substring(p1, textLine.Length - p1);
        }

        // Try to extract movie name from textline
        public string ParseMovie(string textLine)
        {

            if (string.IsNullOrEmpty(textLine) || (textLine.Length > 0 && textLine[0] == '\t'))
                return null;

            int p1 = textLine.IndexOf('\t');
            if (p1 == -1)
                return null;

            // return substring from start to last TAB on line
            return textLine.Substring(0 , p1);
        }

        // determine if is end of data part of file
        protected override bool EndOfExtraction(string textLine)
        {
            if (textLine == null || textLine == endString)
                return true;
            return false;
        }

        // read next record
        protected override string GetNextRecord(StreamReader reader)
        {
            string textLine = null;
            while (true)
            {
                textLine = reader.ReadLine();

                if (textLine == string.Empty || textLine[0] == '\t')
                    continue;

                if (textLine == null || textLine[0] != '\t')
                    return textLine;
            }
        }
    }
}
