﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using parser.Model;
using parser.Repositories;

namespace parser
{
    public class PersonExtractor : Extraction
    {
        public string SourceFilePath;

        // Create new instance and configure by ExtractionConfig
        public PersonExtractor(ExtractionConfig conf)
        {
            if (conf.beginString != null & conf.endString != null)
            {
                this.ExtConfiguration = conf;
                this.beginString = conf.beginString;
                this.endString = conf.endString;
            }
            else
            {
                new Exception("Start/End string is missing");
            }
        }

        // Extract Person obj to repository
        public PersonRepository ExtractToRepository( )
        {
            int id = 1;
            string textLine = null;
            Person person = null;

            HashSet<string> hashSet = new HashSet<string>();
            PersonRepository Rep = new PersonRepository();

            //This add Reader and Writer to use         
            if (!OpenStreams(ExtConfiguration))
                throw new Exception();

            // Scroll reader to start position 
            InitializeStreamPositions();

            //--------------------------------------------------

            foreach (StreamReader currentReader in readers)
            {
                while (!EndOfExtraction(textLine = GetNextRecord(currentReader)))
                {
                    if ((person = ParsePerson(id, textLine)) == null)
                        throw new Exception("Failed to parse Director.");

                    // chceck if already exist in hashset
                    if (!hashSet.Contains(person.name))
                    {
                        hashSet.Add(person.name);
                        Rep.Add(person);
                        id++;
                    }
                }
            }

            CloseStreams();

            return Rep;
        }

        // Extract directors from file
        [System.Obsolete("Use general extractor")]
        public void ExtractDirectors()
        {
            string textLine = null;
            Person person = null;
            HashSet<string> hashSet = new HashSet<string>();
            int id = 1;

            if (!OpenStreams(this.ExtConfiguration))
                throw new Exception();

            InitializeStreamPositions();
            //--------------------------------------------------

            foreach (StreamReader currentReader in readers)
            {
                while (!EndOfExtraction(textLine = GetNextRecord(currentReader)))
                {
                    if ((person = ParsePerson(id,textLine)) == null)
                        throw new Exception("Failed to parse Director.");

                    if (hashSet.Contains(person.name))
                        continue;

                    Console.WriteLine("{0};{1}", person.id, person.name);
                    id++;
                }
            }
            //--------------------------------------------------

            CloseStreams();
        }

        // Try to parse director name from textLine 
        public Person ParsePerson(int id , string textLine)
        {

            if (string.IsNullOrEmpty(textLine) || (textLine.Length > 0 && textLine[0] == '\t'))
                return null;

            int p1 = textLine.IndexOf('\t', 0);
            if (p1 == -1)
                return null;

            return new Person( id , textLine.Substring(0, p1));
        }

        bool ExportListToFile()
        {
            return true;
        }

        // determine if is end of data part of file
        protected override bool EndOfExtraction(string textLine)
        {
            if (textLine == null || textLine == endString)
                return true;
            return false;
        }
        // read next record
        protected override string GetNextRecord(StreamReader reader)
        {
            string textLine = null;
            while (true)
            {
                textLine = reader.ReadLine();

                if (textLine == string.Empty || textLine[0] == '\t')
                    continue;

                if (textLine == null || textLine[0] != '\t')
                    return textLine;
            }
        }
    }
}