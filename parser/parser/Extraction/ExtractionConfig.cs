﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parser
{
    public class ExtractionConfig
    {
        public string[] Sources { get; set; }
        public string Destination { get; set; }

        // Set start of data section
        public string beginString { get; set; }
        // Set end of data section
        public string endString { get; set; }

        public ExtractionConfig(string[] sources, string destination)
        {
            Sources = sources;
            Destination = destination;
        }
        
        // set start and end strings
        public void setStartEndstrings(string start, string end)
        {
            this.beginString = start;
            this.endString = end;
        }

        public ExtractionConfig()
            : this(null, null) { }
    }
}