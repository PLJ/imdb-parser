﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

namespace parser.Binding
{
    public class MovieRatingsBinding : Extraction
    {
        public MoviesRepository MovRep;

        // Hardcode start and end string
        public MovieRatingsBinding(MoviesRepository mov, ExtractionConfig conf)
        {
            beginString = "New  Distribution  Votes  Rank  Title";
            endString = "------------------------------------------------------------------------------";
            MovRep = mov;
            this.ExtConfiguration = conf;

        }

        // Try to extract ratings and bind it
        public MoviesRepository Bind()
        {
            Dictionary<string, Movie> MovDictionary = new Dictionary<string, Movie>();
            foreach (Movie mov in MovRep.Repository)
            {
                //Console.WriteLine(mov.name);
                if (!MovDictionary.ContainsKey(mov.name))
                {
                    MovDictionary.Add(mov.name, mov);
                }
                else
                {
                    // duplicite
                }
            }

            string textLine = null;
            int votes = 0;
            double rating = 0;
            string movie_name = null;
            HashSet<string> hashSet = new HashSet<string>();

            //This add Reader and Writer to use         
            if (!OpenStreams(ExtConfiguration))
                throw new Exception();

            // Scroll reader to start position 
            InitializeStreamPositions();
            //--------------------------------------------------

            foreach (StreamReader currentReader in readers)
            {
                textLine = currentReader.ReadLine();
                textLine = currentReader.ReadLine();

                while (((currentReader.ReadLine()) != null) && (!EndOfExtraction(textLine = GetNextRecord(currentReader))))
                {

                    if ((movie_name = ParseMovie(textLine)) == null)
                        throw new Exception("Failed to parse name.");
                    if ((votes = ParseVotes(textLine)) < 0)
                        throw new Exception("Failed to parse votes.");
                    if ((rating = ParseRating(textLine)) < 0)
                        throw new Exception("Failed to parse rating.");

                    movie_name = movie_name.Trim().Replace("\"", "");
                    if (MovDictionary.ContainsKey(movie_name))
                    {
                        MovRep.Repository[MovDictionary[movie_name].id-1].rating = rating;
                        MovRep.Repository[MovDictionary[movie_name].id-1].votes = votes;
                    }
                }
            }

            CloseStreams();

            return MovRep;
        }

        // Try to parse votes from text line 
        // HardCoded coords
        public int ParseVotes(string textLine)
        {

            int votes = -1;

            if (string.IsNullOrEmpty(textLine) || (textLine.Length > 0 && textLine[0] == '\t'))
                return 0;


            Int32.TryParse(textLine.Substring(16, 8), out votes);
            return votes;
        }


        // Try to rating votes from text line 
        // HardCoded coords
        public double ParseRating(string textLine)
        {
            double rating = -1;
            if (string.IsNullOrEmpty(textLine) || (textLine.Length > 0 && textLine[0] == '\t'))
                return 0;
            Double.TryParse(textLine.Substring(24, 6), out rating);
            return rating;
        }

        // Try to parse movie name from textline
        // From index of two spaces + 2 to end of line 
        public string ParseMovie(string textLine)
        {

            if (string.IsNullOrEmpty(textLine) || (textLine.Length > 0 && textLine[0] == '\t'))
                return null;

            int p1 = textLine.LastIndexOf("  ")+2;
            if (p1 == -1)
                return null;
            int p2 = textLine.Length - textLine.LastIndexOf("  ") - 2;
            if (p2 == -1)
                return null;

            return textLine.Substring(p1, p2);
        }

        // Return true if there is end of data section
        protected override bool EndOfExtraction(string textLine)
        {
            if (textLine == null || textLine == endString)
                return true;
            return false;
        }


        // Read next record
        protected override string GetNextRecord(StreamReader reader)
        {
            string textLine = null;
            while (true)
            {
                textLine = reader.ReadLine();

                if ((textLine == string.Empty || textLine.IndexOf("      ") != 0) && textLine != endString)
                    continue;

                if (textLine == null || textLine[0] != '\t')
                    return textLine;
            }
        }
    }
}
