﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using parser.Repositories;
using parser.Model;
using parser;
using System.IO;
using System.Collections;

namespace parser.Binding
{
    public class PersonPersonBinding
    {
        public PersonRepository PerRepTarget = null;
        public PersonRepository PerRepBind = null;
        
        Dictionary<int, Tuple<List<int>, List<int>>> result = null;

        // Bind Person to Person
        public void bind()
        {
            result = new Dictionary<int, Tuple<List<int>, List<int>>>();
            int i = 0;

            // Create list foreach movie and add persons to it
            foreach (Person target in PerRepTarget.Repository)
            {
                foreach (int movie in target.Movies)
                {
                    if (!result.ContainsKey(movie))
                    {
                        result.Add(movie, new Tuple<List<int>,List<int>>(new List<int>(), new List<int>()));
                        result[movie].Item1.Add(target.id);
                    }
                    else
                    {
                        result[movie].Item1.Add(target.id);
                    }
                }
            }

            // Bind person to person with help of generated lists
            foreach (Person bind in PerRepBind.Repository)
            {
                foreach (int movie in bind.Movies)
                {
                    if (result.ContainsKey(movie))
                    {
                        result[movie].Item2.Add(bind.id);
                    }
                }
            }
        }

        // Save bind to *.csv file
        // Format is recordID;PersonAID;PersonBID
        public void SaveToFile(string path)
        {
            StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Create), Encoding.GetEncoding("Windows-1250"));
            /*
            foreach (KeyValuePair<int,Tuple<int,int>> pair in result)
            {
                writer.WriteLine("{0};{1};{2}", pair.Key, pair.Value.Item1, pair.Value.Item2);
            }
            */
            int index = 1;
            foreach(KeyValuePair<int,Tuple<List<int>, List<int>>> pair in result)
            {
                foreach(int TargetPer in pair.Value.Item1)
                {
                    foreach (int BindPer in pair.Value.Item2)
                    {
                        writer.WriteLine("{0};{1};{2}", index, TargetPer, BindPer);
                        index++;
                    }
                }
            }

            writer.Close();
        }
    }
}
