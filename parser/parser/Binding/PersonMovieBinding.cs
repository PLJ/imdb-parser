﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using parser.Repositories;
using parser.Model;
using parser;
using System.IO;
using System.Collections;

namespace parser.Binding
{
    public class PersonMovieBinding : Extraction
    {
        public PersonRepository PerRep;
        public MoviesRepository MovRep;

        public PersonMovieBinding( PersonRepository per, MoviesRepository mov)
        {
            PerRep = per;
            MovRep = mov;
        }

        // Bind person to movie
        public void Bind(ExtractionConfig conf)
        {
            this.beginString = conf.beginString;
            this.endString = conf.endString;

            if (MovRep == null || PerRep == null)
                throw new Exception("Database arent loaded ...");

            OpenStreams(conf);
            InitializeStreamPositions();

            string textLine = null;
            string MovieName = null;
            string PersonName = null;
            int MovieId = 0;
            int id = 0;
            Person CurrentPerson = null;


            Dictionary<string, Person> PersonDictionary = new Dictionary<string, Person>();
            foreach (Person per in PerRep.Repository)
            {
                //Console.WriteLine(mov.name);
                if (!PersonDictionary.ContainsKey(per.name))
                {
                    PersonDictionary.Add(per.name, per);
                }
                else
                {
                    // duplicite
                }

            }
            Dictionary<string, Movie> MovieDictionary = new Dictionary<string, Movie>();
            foreach (Movie mov in MovRep.Repository)
            {
                //Console.WriteLine(mov.name);
                if(!MovieDictionary.ContainsKey(mov.name))
                {
                    MovieDictionary.Add(mov.name, mov);
                }
                else
                {
                    // duplicita
                }
                        
            }

            foreach (StreamReader currentReader in readers)
            {
                while (!EndOfExtraction(textLine = GetNextRecord(currentReader)))
                {
                    if (textLine[0] == '\t')
                    {
                        // try to extract moviename from textline
                        MovieName = textLine.Substring(textLine.LastIndexOf('\t'), textLine.Length - textLine.LastIndexOf('\t')).Trim().Replace("\"", "");

                        // if ends with > remove <..> from string 
                        if (MovieName[MovieName.Length - 1] == '>')
                        {
                            MovieName = MovieName.Remove(MovieName.LastIndexOf('<'), MovieName.Length - MovieName.LastIndexOf('<'));
                            MovieName = MovieName.Trim();
                        }

                        // if ends with ] removes [...] from string
                        if (MovieName[MovieName.Length - 1] == ']')
                        {
                            MovieName = MovieName.Remove(MovieName.LastIndexOf('['), MovieName.Length - MovieName.LastIndexOf('['));
                            MovieName = MovieName.Trim();
                        }

                        // If contains two spaces extract string from start to index of two spaces
                        if (MovieName.LastIndexOf("  ") > 0)
                        {
                            MovieName = MovieName.Substring(0, MovieName.Length - (MovieName.Length - MovieName.LastIndexOf("  ")));
                        }

                        // Add movie to result if not olreadz in it
                        if (MovieDictionary.ContainsKey(MovieName))
                        {
                            MovieId = MovieDictionary[MovieName].id;
                        }

                        if(MovieId != 0 && !CurrentPerson.Movies.Contains(MovieId))
                        {
                            CurrentPerson.Movies.Add(MovieId);
                        }
                        

                    }
                    else
                    {
                        // person name
                        //Console.WriteLine(textLine.Substring(0, textLine.IndexOf('\t')).Trim());
                        // id = PersonDictionary.FirstOrDefault(x => x.Value == (textLine.Substring(0, textLine.IndexOf('\t')).Trim())).Key;
                        PersonName = textLine.Substring(0, textLine.IndexOf('\t')).Trim();
                        if (PersonDictionary.ContainsKey(PersonName))
                        {
                            CurrentPerson =  PersonDictionary[PersonName];
                        }
                        //CurrentPerson = PerRep.GetByID(id);
                        //id++; // increment for next step
                        //Console.WriteLine(id);

                        // Movie
                        //Console.WriteLine(textLine.Substring(textLine.IndexOf('\t'), textLine.Length-textLine.IndexOf('\t')).Trim());
                        MovieName = textLine.Substring(textLine.IndexOf('\t'), textLine.Length - textLine.IndexOf('\t')).Trim().Replace("\"", "");

                        if (MovieName[MovieName.Length - 1] == '>')
                        {
                            MovieName = MovieName.Remove(MovieName.LastIndexOf('<'), MovieName.Length - MovieName.LastIndexOf('<'));
                            MovieName = MovieName.Trim();
                        }

                        if (MovieName[MovieName.Length - 1] == ']')
                        {
                            MovieName = MovieName.Remove(MovieName.LastIndexOf('['), MovieName.Length - MovieName.LastIndexOf('['));
                            MovieName = MovieName.Trim();
                        }

                        if (MovieDictionary.ContainsKey(MovieName))
                        {
                            MovieId = MovieDictionary[MovieName].id;
                        }
                        if (MovieId != 0)
                        {
                           CurrentPerson.Movies.Add(MovieId);
                        }
                    }
                }
            }

            CloseStreams();
        }
        // read next record
        protected override string GetNextRecord(StreamReader reader)
        {
            string textLine = null;
            while (true)
            {
                textLine = reader.ReadLine();
                if (textLine == string.Empty)
                {
                    continue;
                }
                else
                {
                    return textLine;
                }                
            }
        }
        // determine if is end of data part of file
        protected override bool EndOfExtraction(string textLine)
        {
            if (textLine == null || textLine == endString)
                return true;
            return false;
        }
    }
}
