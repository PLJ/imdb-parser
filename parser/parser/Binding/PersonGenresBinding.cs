﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using parser.Repositories;
using parser.Model;
using parser;
using System.IO;
using System.Collections;

namespace parser.Binding
{
    class PersonGenresBinding
    {
        PersonRepository PerRep = null;
        MoviesRepository MovRep = null;

        // Create new instace with two PersonRepository and Movie repository
        public PersonGenresBinding( PersonRepository PerRep , MoviesRepository MovRep )
        {
            this.PerRep = PerRep;
            this.MovRep = MovRep;
        }

        public void Bind()
        {
            // create hashset for check of existence 
            HashSet<int> Genres = null;

            // For each person For each person movie get genre and add it to hashset, if not yet contained print it
            foreach ( Person per in PerRep.Repository)
            {
                Genres = new HashSet<int>();
                Console.Write(per.id + ";");
                foreach ( int movie_id in per.Movies)
                {
                    //Console.Write(MovRep.GetByID(movie_id).name + ";");
                    foreach ( int genre in MovRep.GetByID(movie_id).genres)
                    {
                        Genres.Add(genre);
                    }                   
                }            
                foreach (int i in Genres)
                {
                    Console.Write( i + ";");
                }
                Console.WriteLine();
            }
        }

        // Bind genres and save to file
        public void BindToFile( string path )
        {
            HashSet<int> Genres = null;
            StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Create), Encoding.GetEncoding("Windows-1250"));
            foreach (Person per in PerRep.Repository)
            {
                Genres = new HashSet<int>();
                writer.Write(per.id + ";");
                foreach (int movie_id in per.Movies)
                {
                    //Console.Write(MovRep.GetByID(movie_id).name + ";");
                    foreach (int genre in MovRep.GetByID(movie_id).genres)
                    {
                        Genres.Add(genre);
                    }
                }
                foreach (int i in Genres)
                {
                    writer.Write(i + ";");
                }
                writer.WriteLine();
            }
            writer.Close();
        }
    }
}
