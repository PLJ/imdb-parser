﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

namespace parser.Binding
{
    public class MovieGenresBinding : Extraction
    {
        public Dictionary<string,int> GenDic;
        public MoviesRepository MovRep;

        // Create new instance of MovieGenresBinding
        // Hardcode start and end string
        public MovieGenresBinding(Dictionary<string, int> gen, MoviesRepository mov, ExtractionConfig conf)
        {
            beginString = "8: THE GENRES LIST";
            endString = "--";
            GenDic = gen;
            MovRep = mov;
            this.ExtConfiguration = conf;

        }

        // Bind Genres to movie
        public MoviesRepository Bind()
        {
            Dictionary<string, Movie> MovDictionary = new Dictionary<string, Movie>();
            foreach (Movie mov in MovRep.Repository)
            {
                //Console.WriteLine(mov.name);
                if (!MovDictionary.ContainsKey(mov.name))
                {
                    MovDictionary.Add(mov.name, mov);
                }
                else
                {
                    // duplicite
                }
            }

            int id = 1;
            string textLine = null;
            string genre_name = null;
            string movie_name = null;
            HashSet<string> hashSet = new HashSet<string>();

            //This add Reader and Writer to use         
            if (!OpenStreams(ExtConfiguration))
                throw new Exception();

            // Scroll reader to start position 
            InitializeStreamPositions();
            //--------------------------------------------------

            foreach (StreamReader currentReader in readers)
            {
                textLine = currentReader.ReadLine();
                textLine = currentReader.ReadLine();

                while (((currentReader.ReadLine()) != null) && (!EndOfExtraction(textLine = GetNextRecord(currentReader))))
                {

                    if ((genre_name = ParseGenre(textLine)) == null)
                        throw new Exception("Failed to parse genre.");

                    if ((movie_name = ParseMovie(textLine)) == null)
                        throw new Exception("Failed to parse genre.");

                    movie_name = movie_name.Trim().Replace("\"", "");
                    if (MovDictionary.ContainsKey(movie_name))
                    {
                        MovRep.Repository[MovDictionary[movie_name].id].genres.Add(GenDic[genre_name]);
                    }
                }
            }

            CloseStreams();

            return MovRep;
        }

        // Parse genre from textline
        // from last index of TAB to end of line
        public string ParseGenre(string textLine)
        {

            if (string.IsNullOrEmpty(textLine) || (textLine.Length > 0 && textLine[0] == '\t'))
                return null;

            int p1 = textLine.LastIndexOf('\t');
            if (p1 == -1)
                return null;

            return textLine.Substring(p1, textLine.Length - p1);
        }

        // Parse movie name
        // from start to index of TAB
        public string ParseMovie(string textLine)
        {

            if (string.IsNullOrEmpty(textLine) || (textLine.Length > 0 && textLine[0] == '\t'))
                return null;

            int p1 = textLine.IndexOf('\t');
            if (p1 == -1)
                return null;

            return textLine.Substring(0, p1);
        }

        // determine if is end of data section of file
        protected override bool EndOfExtraction(string textLine)
        {
            if (textLine == null || textLine == endString)
                return true;
            return false;
        }

        // read next record
        protected override string GetNextRecord(StreamReader reader)
        {
            string textLine = null;
            while (true)
            {
                textLine = reader.ReadLine();

                if (textLine == string.Empty || textLine[0] == '\t')
                    continue;

                if (textLine == null || textLine[0] != '\t')
                    return textLine;
            }
        }
    }
}
