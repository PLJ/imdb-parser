﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using parser.Repositories;
using parser.Model;
using System.IO;


namespace parser.Analysis
{
    public static class Network_stats
    {
        // create egocentric network
        // Gets center id
        // list of movies to include
        // whole person repository
        public static void CreateEgocentricNetwork(int StartID, List<int> Movies, PersonRepository repo)
        {
            Person center = repo.GetByID(StartID);

            Console.WriteLine("Centroid of network is set as person " + center.id + " - "+center.name + " Number of movies:" + center.Movies.Count);


            PersonRepository Network = new PersonRepository();
            Dictionary<int, List<Person>> Result = new Dictionary<int, List<Person>>();

            // Create repo of persons that have atleast one same movie lika center person
            foreach ( Person record in repo.Repository)
            {
                foreach( int searched_movie_id in Movies)
                {
                    if( record.Movies.Contains(searched_movie_id))
                    {
                        if(!Network.Repository.Contains(record))
                        {
                            Network.Add(record);
                            break;
                        }                  
                    }
                }
            }

            // Build the net
            foreach( Person per in Network.Repository)
            {
                foreach (int searched_movie_id in Movies)
                {
                    if (per.Movies.Contains(searched_movie_id))
                    {
                        if (!Result.ContainsKey(searched_movie_id))
                        {
                            Result.Add(searched_movie_id, new List<Person>());
                            Result[searched_movie_id].Add(per);
                        }
                        else
                        {
                            Result[searched_movie_id].Add(per);
                        }

                    }
                }
            }

            // Save net to file
            // Save like undirected graph ready for Gephi
            string path = "./ResultFiles/Ego.csv";
            StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Create), Encoding.GetEncoding("Windows-1250"));
            writer.WriteLine("Source;Target;Type");
            foreach (KeyValuePair<int, List<Person>> record in Result)
            {
                for (int i = 0; i < record.Value.Count - 1; i++)
                {
                    for (int j = i + 1; j < record.Value.Count - 1; j++)
                    {
                        writer.WriteLine(record.Value[i].id + ";" + record.Value[j].id + ";Undirected" );
                    }
                }
            }
            writer.Close();

        }

        public static void CreateGenresMatrix(string path)
        {
            int[] genres = new int[50];
            Dictionary<Tuple<int, int>, int> result = new Dictionary<Tuple<int, int>, int>();
            StreamReader reader = new StreamReader(new FileStream(path, FileMode.Open), Encoding.GetEncoding("Windows-1250"));
            string textLine;
            int index = 0;

            while(!reader.EndOfStream)
            {
                textLine = reader.ReadLine();
                index = 0;
                genres = new int[50];
                foreach (string rec in textLine.Split(';'))
                {
                    if(rec != "")
                    {
                        genres[index] = Convert.ToInt32(rec);
                        index++;
                    }

                }

                for(int i = 1; i < genres.Length-2; i++)
                {
                    for (int j = i+1; j < genres.Length - 1; j++)
                    {
                        if(genres[i] == 0 || genres[j] == 0)
                        {
                            break;
                        }

                        if (genres[i] < genres[j])
                        {
                            if (result.ContainsKey(new Tuple<int, int>(genres[i], genres[j])))
                            {
                                result[new Tuple<int, int>(genres[i], genres[j])] += 1;
                            }
                            else
                            {
                                result.Add(new Tuple<int, int>(genres[i], genres[j]), 1);
                            }
                        }
                        else
                        {
                            if (result.ContainsKey(new Tuple<int, int>(genres[j], genres[i])))
                            {
                                result[new Tuple<int, int>(genres[j], genres[i])] += 1;
                            }
                            else
                            {
                                result.Add(new Tuple<int, int>(genres[j], genres[i]), 1);
                            }
                        }
                    }
                }

            }
            reader.Close();
            Console.WriteLine("Data loaded and graph created.");

            path = "./ResultFiles/graph2.csv";
            StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Create), Encoding.GetEncoding("Windows-1250"));
            writer.WriteLine("Source;Target;Type;Weight");
            foreach (KeyValuePair<Tuple<int, int>, int> record in result)
            {
                writer.WriteLine(record.Key.Item1 + ";" + record.Key.Item2 + ";Undirected;" + record.Value);
            }
            writer.Close();
            Console.WriteLine("Data saved to file.");
        }
    }
}
