﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using parser.Repositories;
using parser.Model;

namespace parser
{
    public static class Repository_stats
    {
        // Print repository stats to console
        public static void PersonRepoStats( PersonRepository repo)
        {
            Console.WriteLine("     Number of records is " + repo.GetCount());
            Console.WriteLine();
            RecordGrade(repo);

        }

        // Calc grade for each record and print
        // For person it is number of movies atached
        public static void RecordGrade(PersonRepository repo)
        {
            Dictionary<int, int> Grades = new Dictionary<int, int>();
            int currentCount;
            double GradesSum = 0;
            Person MaxGrade = new Person(0, "test");


            foreach (Person per in repo.Repository)
            {
                GradesSum = GradesSum + per.Movies.Count();
                if (Grades.TryGetValue(per.Movies.Count(), out currentCount))
                {
                    Grades[per.Movies.Count()] = currentCount + 1;
                }
                else
                {
                    Grades.Add(per.Movies.Count(), 1);
                }             
                
                if(per.Movies.Count > MaxGrade.Movies.Count())
                {
                    MaxGrade = per;
                }

            }

            var Records_count = repo.GetCount();
            double percentage_sum = 0;
            bool top_found = false;
            double top = 95;

            Grades = Grades.OrderBy(pair => pair.Key).ToDictionary(pair => pair.Key, pair => pair.Value);
            foreach ( KeyValuePair<int,int> x in Grades )
            {
                var temp = (double)((double)x.Value / Records_count) * 100;
                temp = (int)(temp * 10000);
                temp = temp / 10000;
                percentage_sum = percentage_sum + temp;
                if ( temp > 0.5 && percentage_sum < top)
                {
                    Console.WriteLine("     Records grade " + x.Key + ": " + x.Value + " : " + temp + "%");
                }            

                if( percentage_sum >= top && top_found == false)
                {
                    Console.WriteLine("     " + top + "% records have grade between 1 and " + x.Key);
                    top_found = true;
                }
            }
            Console.WriteLine();
            Console.WriteLine("     MAX Grade have: " + MaxGrade.name + " with value:" + MaxGrade.Movies.Count());

        }


    }
}
