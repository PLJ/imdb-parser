﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using parser.Repositories;
using static parser.Task;
using parser.Binding;
using parser.Model;

namespace parser
{
    public class TaskManager
    {
        List<Task> TaskList;
        MoviesRepository MovRep;
        Dictionary<string, int> GenresDic;
        Dictionary<int, PersonRepository> PersonRepos;
        string TaskListPath = "";

        // Create new instance of taskmanager 
        // Param contains path to xml file
        public TaskManager(string path)
        {
            TaskListPath = path;
            TaskList = new List<Task>();
            PersonRepos = new Dictionary<int, PersonRepository>();
            LoadTasks();
        }

        // Load task from XML file
        public void LoadTasks()
        {
            string line;
            string[] row = new string[5];
            StreamReader reader = new StreamReader(new FileStream(TaskListPath, FileMode.Open), Encoding.GetEncoding("Windows-1250"));
            Task result = null;
            while ((line = reader.ReadLine()) != null)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Task));
                StringReader rdr = new StringReader(line);
                result = (Task)serializer.Deserialize(rdr);
                TaskList.Add(result);
            }
            reader.Close();
        }

        // Run each extracted task in List<Task> 
        public void RunTasks()
        {
            foreach(Task T in TaskList)
            {
                run(T);
            }

            Console.WriteLine("----------------------");
            Console.WriteLine("All tasks completed.");
        }

        // Run one Task
        // Select proper method for execution based on task type
        public void run(Task T)
        {
            Console.WriteLine("Running task " + T.Name);

            switch (T.Type)
            {
                case TaskType.PersonExtraction:
                    PersonExtraction(T);
                    break;
                case TaskType.MovieLoad:
                    MovieLoad(T);
                    break;
                case TaskType.PersonPersonBinding:
                    PersonPersonBinding(T);
                    break;
                case TaskType.RatingExtraction:
                    RatingExtraction(T);
                    break;
                case TaskType.MovieGenresBinding:
                    GenresExtraction(T);
                    break;
                case TaskType.PersonMovieBinding:
                    PersonMovieBinding(T);
                    break;
                case TaskType.PersonGenresBinding:
                    PersonGenresBinding(T);
                    break;
                case TaskType.PersonRatingBinding:
                    PersonRatingBinding(T);
                    break;

            }

            Console.WriteLine("Task " + T.Name + " completed");
            Console.WriteLine();
        }

        // Method for extraction Person from *.list file
        public void PersonExtraction(Task T)
        {
            ExtractionConfig ExtConfiguration = new ExtractionConfig(new string[] { T.Source }, T.Result);
            SetStringByType(ExtConfiguration, T);
            PersonExtractor PerExtractor = new PersonExtractor(ExtConfiguration);
            PersonRepository PerRep = PerExtractor.ExtractToRepository();

            // create global database of repositories
            PersonRepos.Add(T.Id, PerRep);
            if (true)
            {
                PerRep.SaveToFile(T.Result);
            }

        }
        // Method for extraction Genres from *.list file 
        // Join genres to movies vie MovieGenresBinding
        public void GenresExtraction(Task T)
        {
            ExtractionConfig GenConf = new ExtractionConfig(new string[] { T.Source}, T.Result);
            GenresExtraction GenExt = new GenresExtraction(GenConf);
            GenresDic = GenExt.ExtractToDictionary();
            MovieGenresBinding(T);
        }

        // Method for extraction Person from *.list file
        // Join Rating to movies and save to repository
        // Export to file
        public void RatingExtraction(Task T)
        {
            ExtractionConfig ExtConfiguration = new ExtractionConfig(new string[] { T.Source}, T.Result);
            MovieRatingsBinding MovRat = new MovieRatingsBinding(MovRep, ExtConfiguration);
            MovRep = MovRat.Bind();
            MovRep.SaveToFileRatings(T.Result);
        }

        // Load already extracted movies from *.csv file
        // Save to repository
        public void MovieLoad(Task T)
        {
            MovRep = new MoviesRepository();
            MovRep.LoadFromFile(T.Source);
        }

        // Bind Person to Person and save result to file
        public void PersonPersonBinding(Task T)
        {
            PersonPersonBinding PerPerBind = new Binding.PersonPersonBinding();
            PerPerBind.PerRepTarget = PersonRepos[T.BindingID1];
            PerPerBind.PerRepBind = PersonRepos[T.BindingID2];
            PerPerBind.bind();
            PerPerBind.SaveToFile(T.Result);
        }

        // Bind Person to Movie and save result to file
        public void PersonMovieBinding(Task T)
        {
            PersonMovieBinding PersonMovie = new PersonMovieBinding(PersonRepos[T.BindingID1], MovRep);
            ExtractionConfig ExtConfiguration = new ExtractionConfig(new string[] { T.Source }, "");
            SetStringByType(ExtConfiguration, T);
            PersonMovie.Bind(ExtConfiguration);
            PersonRepos[T.BindingID1].SaveBindToFile(T.Result);
        }

        // Define and set Start and End string based on task name
        public ExtractionConfig SetStringByType( ExtractionConfig conf, Task T)
        {
            switch (T.Name)
            {
                case "ProducersExtraction":
                    conf.setStartEndstrings("----                    ------", "---------------------------------------------------------------------------");
                    break;
                case "ProducersMovieBinding":
                    conf.setStartEndstrings("----                    ------", "---------------------------------------------------------------------------");
                    break;
                case "WritersExtraction":
                    conf.setStartEndstrings("----			------", "---------------------------------------------------------------------");
                    break;
                case "WritersMovieBinding":
                    conf.setStartEndstrings("----			------", "---------------------------------------------------------------------");
                    break;
                case "CostumeDesignersExtraction":
                    conf.setStartEndstrings("----			------", "-----------------------------------------------------------------------------");
                    break;
                case "CostumeDesignersMovieBinding":
                    conf.setStartEndstrings("----			------", "-----------------------------------------------------------------------------");
                    break;
                case "ComposersExtraction":
                    conf.setStartEndstrings("----			------", "-----------------------------------------------------------------------------");
                    break;
                case "ComposersMovieBinding":
                    conf.setStartEndstrings("----			------", "-----------------------------------------------------------------------------");
                    break;
                case "DirectorsMovieBinding":
                    conf.setStartEndstrings("----			------", "-----------------------------------------------------------------------------");
                    break;
                default:
                    conf.setStartEndstrings("----			------", "-----------------------------------------------------------------------------");
                    break;
            }
            return conf;
        }

        // Bind Genres to Person and save result to file
        public void PersonGenresBinding(Task T)
        {
            PersonGenresBinding PerGen = new PersonGenresBinding(PersonRepos[T.BindingID1], MovRep);
            PerGen.BindToFile(T.Result);
        }

        // Bind Genres to Movie and save result to file
        public void MovieGenresBinding(Task T)
        {
            ExtractionConfig GenConf = new ExtractionConfig(new string[] { T.Source }, T.Result);
            MovieGenresBinding MovGenBin = new MovieGenresBinding(GenresDic, MovRep, GenConf);
            MovRep = MovGenBin.Bind();
            MovRep.SaveToFileGenres(T.Result);
        }

        // Bind Rating on Person and save result to file 
        public void PersonRatingBinding(Task T)
        {
            int index = 1;
            StreamWriter writer = new StreamWriter(new FileStream(T.Result, FileMode.Create), Encoding.GetEncoding("Windows-1250"));
            foreach(Person per in PersonRepos[T.BindingID1].Repository)
            {
                foreach( int mov in per.Movies)
                {
                    if (MovRep.GetByID(mov).rating > 0 && MovRep.GetByID(mov).votes > 0)
                    {
                        writer.WriteLine("{0};{1};{2};{3};{4}", index, per.id, mov, MovRep.GetByID(mov).rating, MovRep.GetByID(mov).votes );
                        index++;
                    }
                }
            }
            writer.Close();
        }

    }
}
