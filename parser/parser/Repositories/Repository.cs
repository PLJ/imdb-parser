﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parser
{
    interface Repository<T>
    {

        /* Add new record to repository*/
        void Add(T dir);

        /* Return count of records*/
        int GetCount();

        /*Print all records to Console*/
        void PrintAll();

        /*Print all records to file*/
        void SaveToFile(string path);

        /*Print all records to file*/
        void LoadFromFile(string path);

        /* Return object by ID */
        T GetByID(int index);

        /*Print record by ID */
        void PrintByID( int index);
    }
}
