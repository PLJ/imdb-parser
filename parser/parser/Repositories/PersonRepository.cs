﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using parser.Model;
using System.IO;

namespace parser.Repositories
{
    public class PersonRepository : Repository<Person>
    {
        // Repository contains targets Person objects
        // WIP: should be private and completely maintained by public methods
        public List<Person> Repository;

        // CTOR create new instance
        public PersonRepository()
        {
            Repository = new List<Person>();
        }

        // Add new persont object to repository
        public void Add(Person per)
        {
            Repository.Add(per);
        }

        // Return count of elements
        public int GetCount()
        {
            return Repository.Count();
        }

        // Print ID and NAME of all elements contained in repository
        public void PrintAll()
        {
            foreach (Person per in Repository)
            {
                Console.WriteLine("{0};{1}", per.id, per.name);
            }
        }

        // Save repository to *.csv file
        // Format is ID;Name
        public void SaveToFile(string path)
        {
            StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Create), Encoding.GetEncoding("Windows-1250"));
            foreach (Person per in Repository)
            {
                writer.WriteLine("{0};{1}", per.id, per.name);
            }
            writer.Close();
        }

        // Save repository after Movie bind to *.csv file
        // Format is RecordID;PersonID;MovieID
        public void SaveBindToFile(string path)
        {
            int index = 1;
            StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Create), Encoding.GetEncoding("Windows-1250"));
            foreach (Person per in Repository)
            {
                foreach(int MovId in per.Movies)
                {
                    writer.WriteLine("{0};{1};{2}", index, per.id, MovId);
                    index++;
                }
               
            }
            writer.Close();
        }

        // Load repository from *.csv file
        public void LoadFromFile(string path)
        {
            string line;
            string[] row = new string[5];
            StreamReader reader = new StreamReader(new FileStream(path, FileMode.Open), Encoding.GetEncoding("Windows-1250"));
            while ((line = reader.ReadLine()) != null)
            {
                row = line.Split(';');
                this.Repository.Add(new Person(Convert.ToInt32(row[0]), row[1]));
            }
        }

        // Return record based on Person ID
        public Person GetByID(int index)
        {
           if( (index-1) < this.GetCount() )
            {
                return Repository[index-1];
            }
            return null;
        }

        // Print Person ID and Name to console
        public void PrintByID(int index)
        {
            Console.WriteLine("{0};{1}", Repository[index].id, Repository[index].name);
        }

    }
}
