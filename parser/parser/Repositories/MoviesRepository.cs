﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace parser
{
    public class MoviesRepository : Repository<Movie>
    {

        // Repository contains targets Movie objects
        // WIP: should be private and completely maintained by public methods
        public List<Movie> Repository;

        // CTOR create new instance
        public MoviesRepository()
        {
            Repository = new List<Movie>();
        }

        // Add new object to repository
        public void Add(Movie mov)
        {
            Repository.Add(mov);
        }

        // Return instancy based on ID
        public Movie GetByID(int index)
        {
            return Repository[index-1];
        }
        // Return count of elements
        public int GetCount()
        {
            return Repository.Count();
        }

        // Print ID and NAME of all elements contained in repository
        public void SaveToFile(string path)
        {
            StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Create), Encoding.GetEncoding("Windows-1250"));
            foreach (Movie mov in Repository)
            {
                writer.WriteLine("{0};{1}", mov.id, mov.name);
            }
            writer.Close();
        }
        // Save repository to *.csv file
        // Format is ID;Name;GenresID1;GenresID2 ...
        public void SaveToFileGenres(string path)
        {
            int index = 1;
            StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Create), Encoding.GetEncoding("Windows-1250"));
            foreach (Movie mov in Repository)
            {
                if( mov.genres.Count > 0)
                {
                    writer.Write(index + ";" + mov.id + ";");
                    foreach (int gen in mov.genres)
                    {
                        writer.Write(gen + ";");
                    }
                    writer.WriteLine();
                }
            }
            writer.Close();
        }

        // Save repository to *.csv file
        // Format is ID;Name;Votes;Rating ...
        public void SaveToFileRatings(string path)
        {
            int index = 1;
            StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Create), Encoding.GetEncoding("Windows-1250"));
            foreach (Movie mov in Repository)
            {
                if (mov.rating > 0 && mov.votes > 0)
                {
                    writer.WriteLine(index + ";" + mov.id + ";" + mov.votes + ";" + mov.rating + ";");
                }
                index++;
            }
            writer.Close();
        }

        // Load repository from *.csv file
        public void LoadFromFile(string path)
        {
            string line;
            string[] row = new string[5];
            StreamReader reader = new StreamReader(new FileStream(path, FileMode.Open), Encoding.GetEncoding("Windows-1250"));

            while ((line = reader.ReadLine()) != null)
            {
                row = line.Split(';');
                //Console.WriteLine("{0};{1};{2}", row[0], row[1], row[2]);
                Repository.Add(new Movie(Convert.ToInt32(row[0]), row[1],row[2]));
            }
            reader.Close();
            Console.WriteLine("Load from file: Finished");
        }

        // Return record based on Person ID
        public void PrintAll()
        {
            foreach (Movie mov in Repository)
            {
                Console.WriteLine("{0};{1}", mov.id, mov.name);
            }
        }

        // Print Movie ID and Name to console
        public void PrintByID(int index)
        {
            Console.WriteLine("{0};{1}", Repository[index].id, Repository[index].name);
        }
    }
}
