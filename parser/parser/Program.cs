﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using parser.Repositories;
using parser.Model;
using parser.Binding;
using parser.Analysis;
using System.Xml.Serialization;

namespace parser
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("IMDB Parser - Plesnik Jakub");
            Console.WriteLine("----------------------------------");

            // Load tasks
            TaskManager MM = new TaskManager("./SourceFiles/tasklist.xml");

            // Run
            MM.RunTasks();

            Console.ReadKey();
        }
    }
}
